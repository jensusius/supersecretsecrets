# SuperSecretSecrets

A exercise in learning Rust and Actix by copying the functionality of [ontetimesecret](https://onetimesecret.com/).
The goal of the exercise was to go through all the motions of getting a proof of concept live.

## Key rust libraries

* actix-web for... web stuff
* diesel for db management
* askama for html templating
* openssl for tsl support

## Current setup

* developed on Manjaro
* hosted on a digital ocean droplet, Ubuntu 20
* domain by namecheap
* firewall and DNS setup through digital ocean
* TSL by Let's encrypt
* 'deployment' is currently copying the build artefact to the droplet

## Future considerations

* sort out the logging, possibly with [slog](https://docs.rs/slog/2.5.2/slog/index.html)
* write tests
* set up CI/CD pipeline
