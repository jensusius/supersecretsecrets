CREATE TABLE secrets (
    secret_id TEXT PRIMARY KEY NOT NULL UNIQUE,
    msg TEXT NOT NULL,
    created_at DATETIME NOT NULL,
    burn_at DATETIME NOT NULL
);