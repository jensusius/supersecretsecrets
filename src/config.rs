use actix_files as fs;
use actix_web::web;

pub use config::ConfigError;
use serde::Deserialize;
use slog::{o, Drain, Logger};
use slog_async;
use slog_envlogger;
use slog_term;

use crate::handlers;

#[derive(Deserialize)]
pub struct ServerConfig {
    pub host: String,
    pub port: i32,
}

#[derive(Deserialize)]
pub struct SSLConfig {
    pub cert_path: String,
    pub key_path: String,
}

#[derive(Deserialize)]
pub struct Config {
    pub server: ServerConfig,
    pub ssl: SSLConfig,
}

impl Config {
    pub fn from_env() -> Result<Self, ConfigError> {
        let mut cfg = config::Config::new();
        cfg.merge(config::Environment::new())?;
        cfg.try_into()
    }

    pub fn configure_log() -> Logger {
        let decorator = slog_term::TermDecorator::new().build();
        let console_drain = slog_term::FullFormat::new(decorator).build().fuse();
        let console_drain = slog_envlogger::new(console_drain);
        let console_drain = slog_async::Async::new(console_drain).build().fuse();
        slog::Logger::root(console_drain, o!("v" => env!("CARGO_PKG_VERSION")))
    }
}

pub fn configure_services(cfg: &mut web::ServiceConfig) {
    cfg.service(fs::Files::new("/static", "./static").show_files_listing());
    cfg.service(web::resource("/").route(web::get().to(handlers::index)));
    cfg.service(
        web::resource("/supersecret")
            .name("task_supersecret")
            .route(web::post().to(handlers::post_secret_form)),
    );
    cfg.service(
        web::resource("/supersecret/{secret_id}")
            .route(web::get().to(handlers::read_secret_landing_page))
            .route(web::post().to(handlers::read_secret)),
    );
    cfg.service(
        web::resource("/super/{secret_id}").route(web::get().to(handlers::created_landing_page)),
    );
    cfg.service(
        web::scope("/api").service(
            web::scope("/secret")
                .service(web::resource("").route(web::post().to(handlers::create_secret)))
                .service(web::resource("{secret_id}").route(web::get().to(handlers::get_secret))),
        ),
    );
}
