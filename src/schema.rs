table! {
    secrets (secret_id) {
        secret_id -> Text,
        msg -> Text,
        created_at -> Timestamp,
        burn_at -> Timestamp,
    }
}
