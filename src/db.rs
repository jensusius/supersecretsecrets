// use diesel::prelude::{SqliteConnection};
use diesel::prelude::*;

use chrono::Utc;
use uuid::Uuid;

use crate::errors::{AppError, AppErrorType::*};
use crate::models::Secret;

pub fn get_secret(conn: &SqliteConnection, id: Uuid) -> Result<Secret, AppError> {
    use crate::schema::secrets::dsl::*;

    let mut _secrets = secrets
        .filter(secret_id.eq(id.to_string()))
        .limit(1)
        .load::<Secret>(conn)
        .expect("Error loading secret");

    if _secrets.len() != 1 {
        return Err(AppError {
            msg: Some("No match found".to_string()),
            cause: None,
            err_type: DbError,
        });
    } else {
        match diesel::delete(secrets.filter(secret_id.eq(id.to_string()))).execute(conn) {
            Ok(_) => (),
            Err(e) => {
                return Err(AppError {
                    msg: Some(format!("could not delete the secret: {:?}", e)),
                    cause: None,
                    err_type: DbError,
                })
            }
        }
        Ok(_secrets.pop().unwrap())
    }
}

pub fn create_secret(conn: &SqliteConnection, secret_msg: String) -> Result<Secret, AppError> {
    use crate::schema::secrets::dsl::*;

    let new_secret = Secret {
        secret_id: Uuid::new_v4().to_string(),
        msg: secret_msg,
        created_at: Utc::now().to_rfc3339(),
        burn_at: Utc::now().to_rfc3339(),
    };

    match diesel::insert_into(secrets)
        .values(&new_secret)
        .execute(conn)
    {
        Ok(_) => return Ok(new_secret),
        Err(e) => {
            return Err(AppError {
                msg: Some(format!("could not create the secret: {:?}", e)),
                cause: None,
                err_type: DbError,
            })
        }
    }
}

pub fn is_valid_secret(conn: &SqliteConnection, id: Uuid) -> Result<bool, AppError> {
    use crate::schema::secrets::dsl::*;

    let mut _secrets = secrets
        .filter(secret_id.eq(id.to_string()))
        .limit(1)
        .load::<Secret>(conn)
        .expect("Error loading secret");

    if _secrets.len() == 1 {
        Ok(true)
    } else {
        Ok(false)
    }
}