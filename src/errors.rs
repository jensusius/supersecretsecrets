
#[derive(Debug)]
pub enum AppErrorType {
    DbError,
}

#[derive(Debug)]
pub struct AppError {
    pub msg: Option<String>,
    pub cause: Option<String>,
    pub err_type: AppErrorType,
}
