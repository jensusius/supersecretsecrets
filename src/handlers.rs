use actix_web::{http, web, Error, HttpRequest, Responder};
use askama::Template;
use uuid::Uuid;

use crate::db;
use crate::models::{AppState, CreateSecret};
use crate::templates;

pub async fn get_secret(
    secret_id: web::Path<Uuid>,
    state: web::Data<AppState>,
) -> Result<impl Responder, Error> {
    let secret_id = secret_id.into_inner();
    let conn = state
        .pool
        .get()
        .expect("Could not get db connection from pool");

    let _secret = web::block(move || db::get_secret(&conn, secret_id))
        .await
        .map_err(|_| web::HttpResponse::NotFound().finish())?;

    Ok(web::HttpResponse::Ok().json(_secret))
}

pub async fn create_secret(
    secret: web::Json<CreateSecret>,
    state: web::Data<AppState>,
) -> Result<impl Responder, Error> {
    let conn = state
        .pool
        .get()
        .expect("Could not get db connection from pool");
    let _secret = web::block(move || db::create_secret(&conn, secret.msg.clone()))
        .await
        .map_err(|_| web::HttpResponse::InternalServerError().finish())?;
    Ok(web::HttpResponse::Ok().json(_secret))
}

pub async fn post_secret_form(
    secret: web::Form<CreateSecret>,
    state: web::Data<AppState>,
) -> Result<impl Responder, Error> {
    let conn = state
        .pool
        .get()
        .expect("Could not get db connection from pool");
    let _secret = web::block(move || db::create_secret(&conn, secret.msg.clone()))
        .await
        .map_err(|_| web::HttpResponse::InternalServerError().finish())?;

    Ok(web::HttpResponse::SeeOther()
        .set_header(
            http::header::LOCATION,
            format!("super/{}", _secret.secret_id.to_string()),
        )
        .finish())
}

pub async fn created_landing_page(
    secret_id: web::Path<Uuid>,
    req: HttpRequest,
) -> Result<impl Responder, Error> {
    let path = req.url_for("task_supersecret", &[""])?.into_string();
    println!("{:?}", path);
    let s = templates::SecretGenerated {
        base_path: path,
        secret_id: secret_id.into_inner().to_string(),
    }
    .render()
    .unwrap();
    Ok(web::HttpResponse::Ok().content_type("text/html").body(s))
}

pub async fn read_secret_landing_page(
    secret_id: web::Path<Uuid>,
    state: web::Data<AppState>,
) -> Result<impl Responder, Error> {
    let secret_id = secret_id.into_inner();
    let conn = state
        .pool
        .get()
        .expect("Could not get db connection from pool");
    let is_valid = web::block(move || db::is_valid_secret(&conn, secret_id))
        .await
        .map_err(|_| web::HttpResponse::NotFound().finish())?;

    let mut s = String::new();
    if is_valid == true {
        s = templates::PreViewingSecret {
            secret_id: secret_id.to_string(),
        }
        .render()
        .unwrap();
    } else {
        s = templates::NotValid {}.render().unwrap();
    }
    Ok(web::HttpResponse::Ok().content_type("text/html").body(s))
}

pub async fn read_secret(
    secret_id: web::Path<Uuid>,
    state: web::Data<AppState>,
) -> Result<impl Responder, Error> {
    let secret_id = secret_id.into_inner();
    let conn = state
        .pool
        .get()
        .expect("Could not get db connection from pool");
    let _secret = web::block(move || db::get_secret(&conn, secret_id))
        .await
        .map_err(|_| {
            web::HttpResponse::NotFound()
                .content_type("text/html")
                .body(templates::NotValid {}.render().unwrap())
        })?;

    let s = templates::ViewingSecret { msg: _secret.msg }
        .render()
        .unwrap();
    Ok(web::HttpResponse::Ok().content_type("text/html").body(s))
}

pub async fn index() -> Result<impl Responder, Error> {
    let _index = templates::Index {};
    let s = _index.render().unwrap();
    Ok(web::HttpResponse::Ok().content_type("text/html").body(s))
}
