use diesel::prelude::SqliteConnection;
use diesel::r2d2::{self, ConnectionManager};
use serde::{Deserialize, Serialize};

use crate::schema::secrets;

type DbPool = r2d2::Pool<ConnectionManager<SqliteConnection>>;

#[derive(Clone)]
pub struct AppState {
    pub pool: DbPool,
    pub log: slog::Logger,
}

#[derive(Serialize)]
pub struct Status {
    pub status: String,
}

#[derive(Debug, Clone, Serialize, Queryable, Insertable)]
pub struct Secret {
    pub secret_id: String,
    pub msg: String,
    pub created_at: String,
    pub burn_at: String,
}

#[derive(Serialize, Deserialize)]
pub struct CreateSecret {
    pub msg: String,
}
