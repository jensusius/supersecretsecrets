#[macro_use]
extern crate diesel;

mod config;
mod db;
mod handlers;
mod models;
mod schema;
mod errors;
mod templates;

use actix_web::{middleware, App, HttpServer};
use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
use env_logger::Env;
use slog::info;
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

use crate::config::Config;
use crate::models::AppState;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();
    let config = Config::from_env().unwrap();

    let database_url = std::env::var("DATABASE_URL").expect("DATABSE_URL");
    let manager = ConnectionManager::<SqliteConnection>::new(database_url);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool");

    env_logger::from_env(Env::default().default_filter_or("info")).init();
    // env_logger::init();

    let log = Config::configure_log();

    info!(
        log,
        "Starting server at http://{}:{}", config.server.host, config.server.port
    );

    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder
        .set_private_key_file(config.ssl.key_path, SslFiletype::PEM)
        .unwrap();
    builder.set_certificate_chain_file(config.ssl.cert_path).unwrap();


    // Start HTTP server
    HttpServer::new(move || {
        App::new()
            // set up DB pool to be used with web::Data<Pool> extractor
            .data(AppState {
                pool: pool.clone(),
                log: log.clone(),
            })
            .wrap(middleware::Logger::default())
            .wrap(middleware::Logger::new("%a %{User-Agent}i"))
            .configure(config::configure_services)
    })
    // .bind(format!("{}:{}", config.server.host, config.server.port))?
    .bind_openssl(format!("{}:{}", config.server.host, config.server.port), builder)?
    .run()
    .await
}
