use askama::Template;

#[derive(Template)]
#[template(path = "index.html")]
pub struct Index;

#[derive(Template)]
#[template(path = "secret_generated.html")]
pub struct SecretGenerated {
    pub base_path: String,
    pub secret_id: String,
}

#[derive(Template)]
#[template(path = "pre_view.html")]
pub struct PreViewingSecret {
    pub secret_id: String,
}

#[derive(Template)]
#[template(path = "not_valid.html")]
pub struct NotValid;

#[derive(Template)]
#[template(path = "viewing_secret.html")]
pub struct ViewingSecret {
    pub msg: String,
}
